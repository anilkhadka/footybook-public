﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    public class Game
    {
        public virtual int Id { get; set; }

        public virtual string Round { get; set; }

        public virtual string RecorderName { get; set; }

        public virtual string RecorderEmail { get; set; }

        public virtual DateTime GameDate { get; set; }

        public virtual bool Publish { get; set; }

        public virtual int IdentityId { get; set; }

        public virtual IList<Quarter> Quarters { get; set; }
    }
}
