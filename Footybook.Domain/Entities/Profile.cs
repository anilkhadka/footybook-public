﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    [Serializable]
    public class Profile
    {
        public virtual int IdentityId { get; set; }

        public virtual string ImageUrl { get; set; }

        public virtual float Height { get; set; }

        public virtual int ClubId { get; set; }

        public virtual int DivisionId { get; set; }

        public virtual int LeagueId { get; set; }

        public virtual string Theme { get; set; }

        public virtual Identity Identity { get; set; }

        public virtual DateTime CreatedDate { get; set; }

        public virtual DateTime LastModifiedDate { get; set; }

        public virtual DateTime? DOB { get; set; }

        public virtual float Weight { get; set; }

        public virtual string Shoe { get; set; }

        public virtual int StateId { get; set; }

        public virtual int AgeGroup { get; set; }

        public virtual bool Active { get; set; }
    }
}
