﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    public class Quarter
    {
        public virtual int Id { get; set; }

        public virtual int QuarterNo { get; set; }

        public virtual int Kicks { get; set; }

        public virtual int Handballs { get; set; }

        public virtual int Marks { get; set; }

        public virtual int Tackles { get; set; }

        public virtual int HitOuts { get; set; }

        public virtual int Goals { get; set; }

        public virtual int Behinds { get; set; }

        public virtual int FreeKicksFor { get; set; }

        public virtual int FreeKicksAgainst { get; set; }

        public virtual string Comments { get; set; }

        public virtual Game Game { get; set; }
    }
}
