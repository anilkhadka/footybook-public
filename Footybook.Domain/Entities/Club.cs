﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Footybook.Domain.Entities
{
    [Serializable]
    public class Club
    {
        public virtual int Id { get; set; }
       
        public virtual string ClubName { get; set; }

        public virtual Division Division { get; set; }
    }
}
