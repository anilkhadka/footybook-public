﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    [Serializable]
    public class State
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual IList<League> Leagues { get; set; }

        public State()
        {
            Leagues = new List<League>();
        }
    }
}
