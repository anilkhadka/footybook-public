﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    public class MoveLookUp
    {
        public virtual int Id { get; set; }

        public virtual string MoveType { get; set; }

        public virtual int Points { get; set; }
    }
}
