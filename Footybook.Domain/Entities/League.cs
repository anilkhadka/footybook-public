﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    [Serializable]
    public class League
    {
        public virtual int Id { get; set; }

        public virtual string LeagueName { get; set; }

        public virtual State State { get; set; }

        public virtual IList<Division> Divisions { get; set; }

        public League()
        {
            Divisions = new List<Division>();
        }
    }
}
