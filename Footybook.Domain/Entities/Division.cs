﻿using Footybook.Domain.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    [Serializable]
    public class Division
    {
        public virtual int Id { get; set; }

        public virtual string DivisionName { get; set; }

        public virtual League League { get; set; }

        public virtual IList<Club> Clubs { get; set; }

        public Division()
        {
            Clubs = new List<Club>();
        }
    }
}
