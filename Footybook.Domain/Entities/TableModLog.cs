﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    public class TableModLog
    {
        public virtual int Id { get; set; }
        public virtual string TableName { get; set; }
        public virtual string ColumnName { get; set; }
        public virtual string OldValue { get; set; }
        public virtual string NewValue { get; set; }
        public virtual string UserGuid { get; set; }
        public virtual DateTime ModifiedDateTime { get; set; }
        public virtual string IPAddress { get; set; }
        public virtual string OperatingSystem { get; set; }
    }
}
