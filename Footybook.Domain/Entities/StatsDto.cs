﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    public class StatsDto
    {
        public DateTime GameDate { get; set; }

        public decimal AverageFantasyPoints { get; set; }

        public decimal AverageKicks { get; set; }

        public decimal AverageHandballs { get; set; }

        public decimal AverageGoals { get; set; }

        public decimal AverageBehinds { get; set; }

        public decimal AverageTackles { get; set; }

        public decimal Possessions { get; set; }
    }
}
