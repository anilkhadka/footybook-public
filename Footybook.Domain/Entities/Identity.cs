﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Entities
{
    [Serializable]
    public class Identity
    {
        public virtual int Id { get; set; }

        public virtual string UserGuid { get; set; }

        public virtual string Email { get; set; }

        public virtual string Password { get; set; }

        public virtual string Salt { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string FacebookId { get; set; }

        public virtual Profile Profile { get; set; }

        public virtual DateTime CreatedDate { get; set; }

        public virtual DateTime LastModifiedDate { get; set; }

        public virtual bool Active { get; set; }

        public virtual string TempPassword { get; set; }

        public virtual List<Game> Games { get; set; }

        //public Identity LastUserModified { get; set; }
    }
}
