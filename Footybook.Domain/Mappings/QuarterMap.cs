﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class QuarterMap : ClassMap<Quarter>
    {
        public QuarterMap()
        {
            Table("Quarters");

            Id(x => x.Id);

            Map(x => x.QuarterNo);

            Map(x => x.Kicks);

            Map(x => x.Handballs);

            Map(x => x.Marks);

            Map(x => x.Tackles);

            Map(x => x.HitOuts);

            Map(x => x.Goals);

            Map(x => x.Behinds);

            Map(x => x.FreeKicksFor);

            Map(x => x.FreeKicksAgainst);

            Map(x => x.Comments);

            References(x => x.Game).Column("GameId");
        }
    }
}
