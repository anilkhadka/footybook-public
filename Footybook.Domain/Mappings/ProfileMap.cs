﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class ProfileMap : ClassMap<Profile>
    {
        public ProfileMap()
        {
            //Table("Profile");

            Id(x => x.IdentityId).GeneratedBy.Foreign("Identity");

            Map(x => x.ImageUrl);

            Map(x => x.Height);

            Map(x => x.ClubId);

            Map(x => x.DivisionId);

            Map(x => x.LeagueId);

            Map(x => x.Theme);

            Map(x => x.CreatedDate);

            Map(x => x.LastModifiedDate);

            Map(x => x.DOB);

            Map(x => x.Weight);

            Map(x => x.Shoe);

            Map(x => x.StateId);

            Map(x => x.Active);

            Map(x => x.AgeGroup);

            HasOne(x => x.Identity).Constrained().Cascade.All();
        }
    }
}
