﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class MoveLookUpMap : ClassMap<MoveLookUp>
    {
        public MoveLookUpMap()
        {
            Table("MoveLookUps");

            Id(x => x.Id);

            Map(x => x.MoveType);

            Map(x => x.Points);
        }
    }
}
