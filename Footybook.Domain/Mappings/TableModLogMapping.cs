﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class TableModLogMapping : ClassMap<TableModLog>
    {
        public TableModLogMapping()
        {
            Table("TableModLog");

            Id(x => x.Id);

            Map(x => x.TableName);

            Map(x => x.ColumnName);

            Map(x => x.OldValue);

            Map(x => x.NewValue);

            Map(x => x.UserGuid);

            Map(x => x.ModifiedDateTime);

            Map(x => x.IPAddress);

            Map(x => x.OperatingSystem);
        }
    }
}
