﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class LeagueMap : ClassMap<League>
    {
        public LeagueMap()
        {
            Table("Leagues");

            Id(x => x.Id);

            Map(x => x.LeagueName);

            References(x => x.State)
                .Column("StateId");

            HasMany<Division>(x => x.Divisions)
                .KeyColumn("LeagueId");
        }
    }
}
