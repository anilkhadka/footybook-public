﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class ClubMap : ClassMap<Club>
    {
        public ClubMap()
        {
            Table("Clubs");

            Id(x => x.Id);

            Map(x => x.ClubName);

            References(x => x.Division)
                .Column("DivisionId");
        }
    }
}
