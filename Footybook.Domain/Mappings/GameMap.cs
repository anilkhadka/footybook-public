﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class GameMap : ClassMap<Game>
    {
        public GameMap()
        {
            Table("Games");

            Id(x => x.Id);

            Map(x => x.Round);

            Map(x => x.RecorderName);

            Map(x => x.RecorderEmail);

            Map(x => x.GameDate);

            Map(x => x.Publish);

            Map(x => x.IdentityId);

            HasMany(x => x.Quarters)
                .KeyColumn("GameId")
                .Inverse()
                .Not
                .LazyLoad()
                .Cascade.All();
        }
    }
}
