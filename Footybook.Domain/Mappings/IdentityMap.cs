﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class IdentityMap : ClassMap<Identity>
    {
        public IdentityMap()
        {
            //Table("Identity");

            Id(x => x.Id);

            Map(x => x.UserGuid);

            Map(x => x.Email);

            Map(x => x.Password);

            Map(x => x.TempPassword);

            Map(x => x.Salt);

            Map(x => x.FirstName);

            Map(x => x.LastName);

            Map(x => x.FacebookId);

            Map(x => x.CreatedDate);

            Map(x => x.LastModifiedDate);

            Map(x => x.Active);

            HasOne(x => x.Profile).Cascade.All();
        }
    }
}
