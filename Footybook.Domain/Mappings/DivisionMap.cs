﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class DivisionMap : ClassMap<Division>
    {
        public DivisionMap()
        {
            Table("Divisions");

            Id(x => x.Id);

            Map(x => x.DivisionName);

            References(x => x.League)
                .Column("LeagueId");

            HasMany<Club>(x => x.Clubs)
                .KeyColumn("DivisionId");
        }
    }
}
