﻿using FluentNHibernate.Mapping;
using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Domain.Mappings
{
    public class StateMap : ClassMap<State>
    {
        public StateMap()
        {
            Table("States");

            Id(x => x.Id);

            Map(x => x.Name);

            HasMany<League>(x => x.Leagues)
                .KeyColumn("StateId");
        }
    }
}
