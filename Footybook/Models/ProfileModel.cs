﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models
{
    [Serializable]
    public class ProfileModel
    {
        public int IdentityId { get; set; }

        public string ImageUrl { get; set; }

        public float Height { get; set; }
        
        public ClubModel Club { get; set; }

        public DivisionModel Division { get; set; }

        public LeagueModel League { get; set; }

        public string Theme { get; set; }

        public DateTime DOB { get; set; }

        public float Weight { get; set; }

        public string Shoe { get; set; }

        public StateModel State { get; set; }

        public byte[] UploadedImage { get; set; }

        public IdentityModel Identity { get; set; }

        public int AgeGroup { get; set; }
    }
}