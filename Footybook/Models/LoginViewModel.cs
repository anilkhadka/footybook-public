﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models
{
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}