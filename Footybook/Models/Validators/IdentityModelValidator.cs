﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models.Validators
{
    public class IdentityModelValidator : AbstractValidator<IdentityModel>
    {
        public IdentityModelValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email not provided.");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password not provided.");

            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage("First name not provided.");

            RuleFor(x => x.LastName)
                .NotEmpty()
                .WithMessage("Last name not provided.");

            RuleFor(x => x.AgeGroup)
                .NotEmpty()
                .WithMessage("Age group not provided.");
        }
    }
}