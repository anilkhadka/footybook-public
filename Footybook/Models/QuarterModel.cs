﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models
{
    public class QuarterModel : IComparable<QuarterModel>
    {
        public int Id { get; set; }

        public int QuarterNo { get; set; }

        public int Kicks { get; set; }
        public int KickFantasyPoints { get; set; }

        public int Handballs { get; set; }
        public int HandballFantasyPoints { get; set; }

        public int Marks { get; set; }
        public int MarkFantasyPoints { get; set; }

        public int Tackles { get; set; }
        public int TackleFantasyPoints { get; set; }

        public int HitOuts { get; set; }
        public int HitOutFantasyPoints { get; set; }

        public int Goals { get; set; }
        public int GoalFantasyPoints { get; set; }

        public int Behinds { get; set; }
        public int BehindFantasyPoints { get; set; }

        public int FreeKicksFor { get; set; }
        public int FreeKicksForFantasyPoints { get; set; }

        public int FreeKicksAgainst { get; set; }
        public int FreeKicksAgainstFantasyPoints { get; set; }

        public string Comments { get; set; }

        public int TotalPoints 
        {
            get
            {
                return KickFantasyPoints
                    + HandballFantasyPoints
                    + MarkFantasyPoints
                    + TackleFantasyPoints
                    + HitOutFantasyPoints
                    + GoalFantasyPoints
                    + BehindFantasyPoints
                    + FreeKicksForFantasyPoints
                    + FreeKicksAgainstFantasyPoints; ;
            }
            
        }

        public int CompareTo(QuarterModel otherQuarterModel)
        {
            if (otherQuarterModel == null) return 1;
            if (TotalPoints > otherQuarterModel.TotalPoints) return -1;
            if (TotalPoints < otherQuarterModel.TotalPoints) return 1;
            return 0;
        }
    }
}