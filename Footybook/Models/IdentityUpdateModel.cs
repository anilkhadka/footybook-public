﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models
{
    public class IdentityUpdateModel
    {
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}