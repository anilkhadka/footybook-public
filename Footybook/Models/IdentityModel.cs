﻿using FluentValidation.Attributes;
using Footybook.Web.Models.Validators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models
{
    [Validator(typeof(IdentityModelValidator))]
    [Serializable]
    public class IdentityModel
    {
        public int Id { get; set; }

        [JsonIgnore]
        public string UserGuid { get; set; }

        public string Email { get; set; }
                
        //[JsonIgnore]
        public string Password { get; set; }
        [JsonIgnore]
        public string Salt { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [JsonIgnore]
        public string FacebookId { get; set; }

        public int AgeGroup { get; set; }

        //public ProfileModel Profile { get; set; }

        public List<GameModel> Games { get; set; }
    }

    public class PasswordModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}