﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models
{
    public class StatisticsModel
    {
        public string Year { get; set; }

        public string AverageFantasyPoints { get; set; }

        public decimal FantasyPointsLow { get; set; }

        public decimal FantasyPointsHigh { get; set; }

        public string AverageKicks { get; set; }

        public string AverageHandballs { get; set; }

        public string AverageGoals { get; set; }

        public string AverageBehinds { get; set; }

        public string AverageTackles { get; set; }

        public decimal Possessions { get; set; }
    }
}