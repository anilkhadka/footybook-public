﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Footybook.Web.Models
{
    public class ResponseViewModel
    {
        public object Data { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string[] Message { get; set; }
    }
}