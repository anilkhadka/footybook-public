﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models
{
    public class ClubModel
    {
        public int Id { get; set; }

        public string ClubName { get; set; }
    }
}