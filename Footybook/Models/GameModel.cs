﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models
{
    public class GameModel
    {
        public int Id { get; set; }

        public string Round { get; set; }

        public string RecorderName { get; set; }

        public string RecorderEmail { get; set; }

        public int IdentityId { get; set; }

        public string GameDate { get; set; }

        public List<QuarterModel> Quarters { get; set; }

        public bool Publish { get; set; }

        public int TotalGamePoints 
        {
            get 
            {
                return Quarters.Sum(q => q.TotalPoints);;
            }
        } 
    }
}