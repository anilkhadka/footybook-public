﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Footybook.Web.Models
{
    public class RankModel
    {
        public int IdentityId { get; set; }

        [JsonIgnore]
        public string UserGuid { get; set; }

        public string ImageUrl { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string State { get; set; }

        [JsonIgnore]
        public int TotalPoints { get; set; }
    }
}