﻿using Footybook.Services.Impls;
using Footybook.Web.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Footybook.Providers
{
    //public class FootybookAuthorizationServerProvider : OAuthAuthorizationServerProvider
    //{
    //    public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
    //    {
    //        context.Validated();
    //    }

    //    public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
    //    {
    //        context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

    //        var _userRepo = new IdentityRepository();
    //        var user = await _userRepo.FindByEmail(context.UserName); //context.UserName is the Email value

    //        if (user == null)
    //        {
    //            SetError(context);
    //            return;
    //        }

    //        var passwordManager = new PasswordManager();
    //        var IsMatch = passwordManager.MatchPassword(context.Password, user.Salt, user.Password);

    //        if(!IsMatch)
    //        {
    //            SetError(context);
    //            return;
    //        }

    //        var identity = new ClaimsIdentity(context.Options.AuthenticationType);
    //        identity.AddClaim(new Claim(ClaimTypes.Name, user.UserGuid));
    //        identity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
            
    //        context.Validated(identity);
    //    }

    //    private void SetError(OAuthGrantResourceOwnerCredentialsContext context)
    //    {
    //        context.SetError("invalid_grant", "The user name or password is incorrect.");
    //    }
    //}
}