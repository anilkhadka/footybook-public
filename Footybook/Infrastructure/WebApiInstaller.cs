﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Footybook.Services.Impls;
using Footybook.Services.Interfaces;
using Footybook.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Footybook.Web.Infrastructure
{
    public class WebApiInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            // Register all the dependencies
            container.Register(Component.For<IIdentityRepository>()
                .ImplementedBy<IdentityRepository>()
                .LifestyleScoped());

            container.Register(Component.For<IProfileRepository>()
                .ImplementedBy<ProfileRepository>()
                .LifestyleScoped());

            container.Register(Component.For<ITableModLogRepository>()
                .ImplementedBy<TableModLogRepository>()
                .LifestyleScoped());

            container.Register(Component.For<IClubRepository>()
                .ImplementedBy<ClubRepository>()
                .LifestyleScoped());

            container.Register(Component.For<IGameRepository>()
                .ImplementedBy<GameRepository>()
                .LifestyleScoped());

            container.Register(Component.For<IMoveLookUpRepository>()
                .ImplementedBy<MoveLookUpRepository>()
                .LifestyleScoped());

            // Register all the Web Api controllers within this assembly
            container.Register(Classes.FromAssembly(typeof(IdentityController).Assembly)
                                      .BasedOn<ApiController>()
                                      .LifestyleScoped());
            
        }
    }
}