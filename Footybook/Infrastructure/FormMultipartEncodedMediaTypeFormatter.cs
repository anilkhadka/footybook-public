﻿using Footybook.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Footybook.Web.Infrastructure
{
    public class FormMultipartEncodedMediaTypeFormatter : FormUrlEncodedMediaTypeFormatter
    {
        private const string StringMultipartMediaType = "multipart/form-data";
        private const string StringApplicationMediaType = "application/octet-stream";

        public FormMultipartEncodedMediaTypeFormatter()
        {
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue(StringMultipartMediaType));
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue(StringApplicationMediaType));
        }

        public override bool CanReadType(Type type)
        {
            return type == typeof(FileModel);
        }

        public override bool CanWriteType(Type type)
        {
            return false;
        }

        public async override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            if (!content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var fileParts = await content.ReadAsMultipartAsync();
            //var fileContent = fileParts.Contents.First(x => this.SupportedMediaTypes.Contains(x.Headers.ContentType));
            var fileContent = fileParts.Contents.First();

            var dataAsString = "";
            foreach (var part in fileParts.Contents.Where(x => x.Headers.ContentDisposition.DispositionType == "form-data"
                                                        && x.Headers.ContentDisposition.Name == "\"data\""))
            {
                var data = await part.ReadAsStringAsync();
                dataAsString = data;
            }

            string fileName = fileContent.Headers.ContentDisposition.FileName;
            string mediaType = fileContent.Headers.ContentType.MediaType;

            using (var imgAsStream = await fileContent.ReadAsStreamAsync())
            {
                byte[] imageBuffer = ReadFully(imgAsStream);
                return new FileModel(fileName, dataAsString.Length, imageBuffer);
            }
        }

        private byte[] ReadFully(Stream input)
        {
            var Buffer = new byte[16 * 1024];
            using (var Ms = new MemoryStream())
            {
                int Read;
                while ((Read = input.Read(Buffer, 0, Buffer.Length)) > 0)
                {
                    Ms.Write(Buffer, 0, Read);
                }
                return Ms.ToArray();
            }
        }
    }
}