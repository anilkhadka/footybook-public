﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Net;
using System.Text;

namespace Footybook.Web.Email
{
    public class EmailSender
    {
        private SmtpClient _smtpClient;

        public EmailSender()
        {
            _smtpClient = new SmtpClient();
        }

        public bool SendEmail(MailMessage emailMessage)
        {
            var emailSuccessfullySent = true;

            try
            {
                _smtpClient.Send(emailMessage);
            }
            catch (SmtpException ex)
            {
                emailSuccessfullySent = false;
                throw ex;
            }

            return emailSuccessfullySent;
        }

        public string ConstructPasswordResetEmailBody(out string tempPassword)
        {
            //var currentRequest = HttpContext.Current.Request;
            //var rootDomain = currentRequest.Url.Scheme + "://" + currentRequest.Url.Host;

            tempPassword = System.Guid.NewGuid().ToString().Substring(0, 8);

            var emailBody = new StringBuilder();
            emailBody.AppendLine("<HTML>");
            emailBody.AppendLine("<BODY>");
            emailBody.AppendLine("<P>");
            emailBody.AppendLine("Please find following temporary password for your <b>Footybook</b> application: <BR />");
            emailBody.AppendLine(string.Format("{0}: <b>{1}</b>", "Temporary Password", tempPassword));
            emailBody.AppendLine("<BR />");
            emailBody.AppendLine("</P>");
            emailBody.AppendLine("</BODY>");
            emailBody.AppendLine("</HTML>");

            return emailBody.ToString();
        }
    }
}