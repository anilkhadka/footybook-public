﻿using Footybook.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace Footybook.Web.Security
{
    public class PasswordManager
    {
        /// <summary>
        /// Generate hashed password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public string GenerateHashedPassword(string email, string password, out string salt)
        {
            salt = GenerateSalt(email);

            return HashPassword(password, salt);
        }

        /// <summary>
        /// Validate password with existing password for the user
        /// </summary>
        /// <param name="password"></param>
        /// <param name="existingPassword"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public bool ValidatePassword(string password, string existingPassword, string salt)
        {
            var hashedPassword = HashPassword(password, salt);

            return CompareTwoHashedPaswords(Utility.GetByteArrayFromString(hashedPassword), Utility.GetByteArrayFromString(existingPassword));
        }

        private static string GenerateSalt(string email)
        {
            Rfc2898DeriveBytes hasher = new Rfc2898DeriveBytes(email,
                System.Text.Encoding.Default.GetBytes("perthwebapp"), 10000);
            return Convert.ToBase64String(hasher.GetBytes(25));
        }

        private static string HashPassword(string password, string salt)
        {
            Rfc2898DeriveBytes hasher = new Rfc2898DeriveBytes(password,
                System.Text.Encoding.Default.GetBytes(salt), 10000);
            return Convert.ToBase64String(hasher.GetBytes(25));
        }

        private static bool CompareTwoHashedPaswords(byte[] password1, byte[] password2)
        {
            bool areSame = true;

            if (password1.Length != password2.Length)
                return false;

            //Compare the values of the two byte arrays. 
            for (int x = 0; x < password1.Length; x++)
            {
                if (password1[x] != password2[x])
                {
                    areSame = false;
                }
            }

            return areSame;
        }
    }
}