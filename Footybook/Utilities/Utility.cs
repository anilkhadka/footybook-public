﻿using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Footybook.Web.Utilities
{
    public static class Utility
    {
        public static string GetStringFromByteArray(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public static byte[] GetByteArrayFromString(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetRandomUserGuid()
        {
            byte[] buffer = Guid.NewGuid().ToByteArray();
            var FormNumber = BitConverter.ToUInt32(buffer, 0) ^ BitConverter.ToUInt32(buffer, 4) ^ BitConverter.ToUInt32(buffer, 8) ^ BitConverter.ToUInt32(buffer, 12);
            return FormNumber.ToString("X");
        }

        public static DateTime ToFormattedDateTime(this DateTime dateTime, string format)
        {
            return DateTime.ParseExact(dateTime.ToString(format), format, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Returns properties whose values are different from each other
        /// </summary>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <returns></returns>
        public static Dictionary<string, ValueHolder> GetPropertiesWithDifferentValues(Identity source, Identity dest)
        {
            var propertyList = new Dictionary<string, ValueHolder>();

            if (source == null || dest == null)
                return propertyList;

            if (!string.IsNullOrWhiteSpace(source.Email) && !string.IsNullOrWhiteSpace(dest.Email))
            { 
                if(source.Email.ToLower() != dest.Email.ToLower())
                    propertyList.Add(source.Email, new ValueHolder { OldValue = source.Email, NewValue = dest.Email });
            }

            if(!string.IsNullOrWhiteSpace(source.FirstName) && !string.IsNullOrWhiteSpace(dest.FirstName))
            {
                if(source.FirstName.ToLower() != dest.FirstName.ToLower())
                    propertyList.Add(source.FirstName, new ValueHolder{ OldValue = source.FirstName, NewValue = dest.FirstName});
            }

            if(!string.IsNullOrWhiteSpace(source.LastName) && !string.IsNullOrWhiteSpace(dest.LastName))
            {
                if(source.LastName.ToLower() == dest.LastName.ToLower())
                {
                    propertyList.Add(source.LastName, new ValueHolder{ OldValue = source.LastName, NewValue = dest.LastName});
                }
            }

            return propertyList;
        }
    }

    public class ValueHolder
    {
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}