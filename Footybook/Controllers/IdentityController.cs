﻿using AutoMapper;
using Footybook.Domain.Entities;
using Footybook.Services.Interfaces;
using Footybook.Web.Models;
using Footybook.Web.Security;
using Footybook.Web.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Footybook.Web.Email;
using System.Configuration;
using Footybook.Web.Extensions;
using Footybook.Services.Impls;

namespace Footybook.Web.Controllers
{
    [RoutePrefix("api/Identity")]
    public class IdentityController : ApiController
    {
        private readonly IIdentityRepository _identityRepo;
        private readonly ITableModLogRepository _tableModLogRepo;
        private readonly IClubRepository _clubRepo;
        private readonly IGameRepository _gameRepo;
        
        public IdentityController(IIdentityRepository identityRepository, ITableModLogRepository tableModLogRepository, IClubRepository clubRepository, IGameRepository gameRepository)
        {
            _identityRepo = identityRepository;
            _tableModLogRepo = tableModLogRepository;
            _clubRepo = clubRepository;
            _gameRepo = gameRepository;
        }

        [HttpPost, Route("Login"), AllowAnonymous]
        public ResponseViewModel Login([FromBody] LoginViewModel model)
        {
            var response = new ResponseViewModel();

            if (model == null)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] {"Request data can not be null. "};
                return response;
            }

            var user = _identityRepo.FindByEmail(model.Email);

            if (user == null)
            {
                response.StatusCode = HttpStatusCode.Forbidden;
                response.Message = new string[] { "Email does not exist." };
                return response;
            }

            if (user != null && !user.Active)
            {
                response.StatusCode = HttpStatusCode.Forbidden;
                response.Message = new string[] { "User not active." };
                return response;
            }

            var passwordManager = new PasswordManager();
            var isMatch = passwordManager.ValidatePassword(model.Password, user.Password, user.Salt);

            //check if the TemporaryPassword field is active, send ResponseStatus:302 if active
            if (!string.IsNullOrWhiteSpace(user.TempPassword))
            {
                if(user.TempPassword.Equals(model.Password, StringComparison.OrdinalIgnoreCase))
                    isMatch = true;
            }

            if (!isMatch)
            {
                response.StatusCode = HttpStatusCode.Forbidden;
                response.Message = new string[] { "Password does not match." };
                return response;
            }

            var token = CreateClaimsIdentity(model.Email, user.UserGuid);

            response.StatusCode = HttpStatusCode.OK;
            response.Data = new { Token = token, IdentityId = user.Id, FirstName = user.FirstName, LastName = user.LastName };
            response.Message = new string[]{"Login successful."};
            return response;
        }

        [HttpGet, Route("Logout")]
        public HttpResponseMessage Logout()
        {
            var authManager = Request.GetOwinContext().Authentication;
            authManager.SignOut(DefaultAuthenticationTypes.ExternalBearer);

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpPost, Route("Register"), AllowAnonymous]
        public ResponseViewModel Create([FromBody]IdentityModel model)
        {
            var response = new ResponseViewModel();

            if (model == null)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { "Request data can not be null. "};
                return response;
            }

            //check if the email already exists
            var existingUser = _identityRepo.FindByEmail(model.Email);

            if (existingUser != null)
            {
                response.StatusCode = HttpStatusCode.Conflict;
                response.Message = new string[]{ "Email already exists." };
                return response;
            }

            var identity = Mapper.Map<Identity>(model);
            var profile = new Footybook.Domain.Entities.Profile();

            //Now, hash password before saving to the database
            var salt = string.Empty;
            var passwordManager = new PasswordManager();
            identity.Password = passwordManager.GenerateHashedPassword(model.Email, model.Password, out salt);
            
            identity.Salt = salt;
            identity.Profile = profile;
            identity.UserGuid = Utility.GetRandomUserGuid();
            identity.CreatedDate = DateTime.UtcNow.ToFormattedDateTime("MM-dd-yyyy HH:mm:ss");
            identity.Active = true;
            identity.LastModifiedDate = DateTime.UtcNow.ToFormattedDateTime("MM-dd-yyyy HH:mm:ss"); 

            profile.Identity = identity;
            profile.CreatedDate = DateTime.UtcNow.ToFormattedDateTime("MM-dd-yyyy HH:mm:ss");
            profile.LastModifiedDate = DateTime.UtcNow.ToFormattedDateTime("MM-dd-yyyy HH:mm:ss");
            profile.AgeGroup = model.AgeGroup;
            profile.Active = true;

            var newIdentity = _identityRepo.Add(identity);

            var responseData = Mapper.Map<IdentityModel>(newIdentity);
            responseData.AgeGroup = newIdentity.Profile.AgeGroup;

            response.Data = responseData;
            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[]{"User registered successfully."};
            return response;
        }

        [HttpPut, Route("Modify")]
        public ResponseViewModel Update([FromBody]IdentityUpdateModel model)
        {
            var response = new ResponseViewModel();

            if (model == null)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { "Request data can not be null. " };
                return response;
            }

            //Ger user from claim
            var claimIdentity = (ClaimsIdentity)User.Identity;
            var userGuid = claimIdentity.Claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            var identity = _identityRepo.GetByUserGuid(userGuid.Value);

            if (identity == null)
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.Message = new string[] { "User not found. " };
                return response;
            }

            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                //Now, hash password before saving to the database
                var salt = string.Empty;
                var passwordManager = new PasswordManager();
                identity.Password = passwordManager.GenerateHashedPassword(identity.Email, model.Password, out salt);
                identity.Salt = salt;
            }
            
            if(!string.IsNullOrWhiteSpace(model.FirstName))
                identity.FirstName = model.FirstName;

            if(!string.IsNullOrWhiteSpace(model.LastName))
                identity.LastName = model.LastName;

            identity.LastModifiedDate = DateTime.UtcNow.ToFormattedDateTime("MM-dd-yyyy HH:mm:ss");
                        
            var updatedIdentity = _identityRepo.Update(identity);

            var responseData = Mapper.Map<IdentityUpdateModel>(updatedIdentity);
            responseData.Password = string.Empty;
            
            response.Data = responseData;
            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[] { "User details has been updated successfully." };

            return response;
        }

        [HttpGet]
        public ResponseViewModel Get(int id)
        {
            var response = new ResponseViewModel();

            if (id == 0)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { "Id not found." };
                return response;
            }
                
            var identity = _identityRepo.GetById(id);

            var responseData = Mapper.Map<IdentityModel>(identity);
            responseData.AgeGroup = identity.Profile.AgeGroup;

            response.Data = responseData;
            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[]{"Profile found."};
            return response;
        }

        [HttpGet, Route("ResetPassword"),AllowAnonymous]
        public ResponseViewModel ResetPassword(string email)
        {
            var response = new ResponseViewModel();

            if (string.IsNullOrWhiteSpace(email))
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { "Email not provided."};
                return response;
            }

            var user = _identityRepo.FindByEmail(email);

            if (user == null)
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.Message = new string[] {"Email does not exist."};
                return response;
            }

            var appSettings = System.Web.Configuration.WebConfigurationManager.AppSettings;

            if (appSettings.Count > 0)
            {
                var from = appSettings["companyEmail"];

                if (from == null)
                    throw new Exception("No company email configured.");

                var emailMessage = new MailMessage();
                emailMessage.From = new MailAddress(from);
                emailMessage.To.Add(new MailAddress(email));
                emailMessage.Subject = "Reset Password";
                emailMessage.IsBodyHtml = true;

                var emailSender = new EmailSender();

                string tempPassword = string.Empty;
                emailMessage.Body = emailSender.ConstructPasswordResetEmailBody(out tempPassword);

                //First, update the temporary password column
                user.TempPassword = tempPassword;
                _identityRepo.Update(user);

                //Then, send the email with the temporary password
                emailSender.SendEmail(emailMessage);
            }

            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[] { string.Format("Temporary password has been sent to: {0}", email) };
            return response;
        }

        [HttpPost, Route("ResetPassword"),AllowAnonymous]
        public ResponseViewModel ResetPassword(PasswordModel model)
        {
            var response = new ResponseViewModel();

            if (model == null)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[]{"Not data sent with the request."};
                return response;
            }
                
            var user = _identityRepo.FindByEmail(model.Email);

            if (user == null)
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.Message = new string[]{ "User does not exist."};
                return response;
            }

            if (model.Password.ToLower() != model.ConfirmPassword.ToLower())
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { "Password and Confirm Password fields do not match." };
                return response;
            }

            //if all good then update the password and delete the temporary field
            user.Password = model.Password;
            user.TempPassword = string.Empty;
            _identityRepo.Update(user);

            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[] { "Password has been reset successfully." };

            return response;
        }

        [Route("Ranks"), HttpGet]
        public ResponseViewModel Ranks(string query = "")
        {
            var response = new ResponseViewModel();

            //Get user from claim
            var claimIdentity = (ClaimsIdentity)User.Identity;
            var userGuid = claimIdentity.Claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
            var identity = _identityRepo.GetByUserGuid(userGuid.Value);

            var identities = _identityRepo.GetAllUsersByAgeGroup(identity.Profile.AgeGroup, query);
            var profileDetails = identities.Select(i => i.Profile);
            var stateList = _clubRepo.GetAllStates();
                                    
            var models = new List<IdentityModel>();

            if (identities.Any())
            {
                foreach (var i in identities)
                {
                    var model = Mapper.Map<IdentityModel>(i);
                    models.Add(model);
                }
            }

            var rankModels = models.Select(m => new RankModel {
                IdentityId = m.Id,
                UserGuid = m.UserGuid,
                FirstName = m.FirstName,
                LastName = m.LastName
            }).ToList();


            //Now, add up the total points to order by descending
            if (identities.Any())
            {
                var identityIds = identities.Select(i => i.Id).ToArray<int>();
                var games = _gameRepo.GamesByIdentityIds(identityIds);

                foreach (var i in identities)
                {
                    var gamesPerIdentity = games.Where(g => g.IdentityId == i.Id);

                    if (gamesPerIdentity.Any())
                    {
                        var totalPoints = gamesPerIdentity.SelectMany(g => g.Quarters).Sum(q => q.Behinds + q.Handballs + q.Tackles + q.HitOuts + q.Kicks + q.FreeKicksFor);

                        foreach (var r in rankModels)
                        {
                            if (r.IdentityId == i.Id)
                            {
                                r.TotalPoints = totalPoints;
                            }
                        }
                    }
                }
            }

            //Now, update the ranks with ImageUrl and State from profileDetails
            if (rankModels.Count() == profileDetails.Count())
            {
                foreach (var rm in rankModels)
                {
                    foreach (var p in profileDetails)
                    {
                        if (p.Identity.Id.Equals(rm.IdentityId))
                        {
                            rm.ImageUrl = p.ImageUrl;
                            rm.State = stateList.FirstOrDefault(s => s.Id == p.StateId) != null ? stateList.FirstOrDefault(s => s.Id == p.StateId).Name : string.Empty;
                        }
                    }
                }
            }

            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[] { "Ranks returned successfully." };
            response.Data = rankModels.OrderByDescending(rm => rm.TotalPoints).ToList();

            return response;
        }

        private string CreateClaimsIdentity(string email, string userGuid)
        {
            var accessToken = string.Empty;
            var identity = new ClaimsIdentity(Startup.OAuthBearerOptions.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, email));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userGuid));
            AuthenticationTicket ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
            var currentUtc = DateTime.UtcNow;
            ticket.Properties.IssuedUtc = DateTime.UtcNow;
            ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromHours(24));
            accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);
            return accessToken;
        }
    }
}