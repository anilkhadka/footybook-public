﻿using AutoMapper;
using Footybook.Domain.Entities;
using Footybook.Services.Interfaces;
using Footybook.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Footybook.Web.Controllers
{
    [RoutePrefix("api")]
    public class GameController : ApiController
    {
        private IGameRepository _gameRepository;
        private IMoveLookUpRepository _moveLookUpRepository;
        private IIdentityRepository _identityRepository;

        public GameController(IGameRepository gameRepository, IMoveLookUpRepository moveLookUpRepository, IIdentityRepository identityRepository)
        {
            _gameRepository = gameRepository;
            _moveLookUpRepository = moveLookUpRepository;
            _identityRepository = identityRepository;
        }

        [HttpGet, Route("Blackboard")]
        public ResponseViewModel Games(int identityId)
        {
            var response = new ResponseViewModel();

            var gameModels = new List<GameModel>();
            var fantasyPoints = _moveLookUpRepository.GetAll();

            //Ger user from claim
            var claimIdentity = (ClaimsIdentity)User.Identity;
            var userGuid = claimIdentity.Claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            var identity = _identityRepository.GetByUserGuid(userGuid.Value);

            var isCurrentUser = identity.Id == identityId;

            var games = _gameRepository.GamesByIdentityId(identityId, isCurrentUser);

            foreach(var game in games)
            {
                var gameModel = Mapper.Map<GameModel>(game);

                if (game.GameDate != null)
                    gameModel.GameDate = game.GameDate.ToString("dd/MM/yyyy");

                if (gameModel.Quarters.Any())
                {
                    foreach (var q in gameModel.Quarters)
                    {
                        q.BehindFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Behinds", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Behinds;
                        q.FreeKicksAgainstFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("FreeKicksAgainst", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.FreeKicksAgainst;
                        q.FreeKicksForFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("FreeKicksFor", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.FreeKicksFor;
                        q.GoalFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Goals", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Goals;
                        q.HandballFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Handballs", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Handballs;
                        q.HitOutFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("HitOuts", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.HitOuts;
                        q.KickFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Kicks", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Kicks;
                        q.MarkFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Marks", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Marks;
                        q.TackleFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Tackles", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Tackles;
                    }
                }

                gameModels.Add(gameModel);
            }

            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[] { "Data returned successfully." };
            response.Data = gameModels;
            return response;
        }

        [Route("Statistics"), HttpGet]
        public ResponseViewModel Statistics(int identityId)
        {
            var response = new ResponseViewModel();

            var statsDtoList = _gameRepository.GamesPerYearPerUser(identityId);

            var statisticsModelList = new List<StatisticsModel>();
            var fantasyPoints = _moveLookUpRepository.GetAll();

            if (statsDtoList.Any()) 
            {
                statisticsModelList.AddRange(statsDtoList.Select(s => new StatisticsModel
                {
                    Year = s.GameDate.Year.ToString(),
                    AverageFantasyPoints = s.AverageFantasyPoints.ToString("#.#0"),
                    AverageKicks = s.AverageKicks.ToString("#.#0"),
                    AverageHandballs = s.AverageHandballs.ToString("#.#0"),
                    AverageGoals = s.AverageGoals.ToString("#.#0"),
                    AverageBehinds = s.AverageBehinds.ToString("#.#0"),
                    AverageTackles = s.AverageTackles.ToString("#.#0"),
                    Possessions = s.Possessions
                }).ToList());
            }

            //Now, calculate the FantasyPointsLow and FantasyPointsHigh
            var games = _gameRepository.GamesByIdentityId(identityId, false);

            var fantasyPointsPerGame = new List<Stat>();

            if (games.Any())
            {
                foreach (var g in games)
                {
                    var gameModel = Mapper.Map<GameModel>(g);

                    foreach (var q in gameModel.Quarters)
                    {
                        q.BehindFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Behinds", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Behinds;
                        q.FreeKicksAgainstFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("FreeKicksAgainst", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.FreeKicksAgainst;
                        q.FreeKicksForFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("FreeKicksFor", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.FreeKicksFor;
                        q.GoalFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Goals", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Goals;
                        q.HandballFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Handballs", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Handballs;
                        q.HitOutFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("HitOuts", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.HitOuts;
                        q.KickFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Kicks", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Kicks;
                        q.MarkFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Marks", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Marks;
                        q.TackleFantasyPoints = (fantasyPoints
                            .FirstOrDefault(fp => String.Equals("Tackles", fp.MoveType, StringComparison.OrdinalIgnoreCase)).Points) * q.Tackles;
                    }

                    fantasyPointsPerGame.Add(new Stat {
                        GameDate = DateTime.Parse(gameModel.GameDate),
                        TotalFantasyPoints = gameModel.Quarters.Sum(q => q.TotalPoints)
                    });

                }

                if (fantasyPointsPerGame.Any() && statisticsModelList.Any())
                {
                    foreach (var fpg in fantasyPointsPerGame)
                    {
                        foreach (var sml in statisticsModelList)
                        {
                            if (!string.IsNullOrWhiteSpace(sml.Year))
                            {
                                var year = int.Parse(sml.Year);
                                if (year.Equals(fpg.GameDate.Year))
                                {
                                    var matchingFantasyPointsPerYear = fantasyPointsPerGame
                                        .Where(f => f.GameDate.Equals(fpg.GameDate));

                                    sml.FantasyPointsLow = matchingFantasyPointsPerYear.Min(fp => fp.TotalFantasyPoints);
                                    sml.FantasyPointsHigh = matchingFantasyPointsPerYear.Max(fp => fp.TotalFantasyPoints);
                                }
                            }
                        }
                    }
                }

                if (games.Any() && statisticsModelList.Any())
                {
                    foreach (var sml in statisticsModelList)
                    {
                        var gameYear = int.Parse(sml.Year);
                        //Now, update the average data
                        var matchingGames = games.Where(game => game.GameDate.Year.Equals(gameYear));

                        if (matchingGames.Any() && matchingGames.Count() > 0)
                        {
                            var totalGames = matchingGames.Count();
                            sml.AverageKicks = (Convert.ToDecimal(sml.AverageKicks) / totalGames).ToString("#.#0");
                            sml.AverageBehinds = (Convert.ToDecimal(sml.AverageBehinds) / totalGames).ToString("#.#0");
                            sml.AverageFantasyPoints = (Convert.ToDecimal(sml.AverageFantasyPoints) / totalGames).ToString("#.#0");
                            sml.AverageGoals = (Convert.ToDecimal(sml.AverageGoals) / totalGames).ToString("#.#0");
                            sml.AverageHandballs = (Convert.ToDecimal(sml.AverageHandballs) / totalGames).ToString("#.#0");
                            sml.AverageTackles = (Convert.ToDecimal(sml.Possessions) / totalGames).ToString("#.#0");
                        }
                    }
                }
            }

            response.Data = statisticsModelList;
            response.StatusCode = HttpStatusCode.OK;
            response.Message = new []{"Player stats sent successfully."};
            
            return response;
        }

        [Route("Games/Create"), HttpPost]
        public ResponseViewModel Add([FromBody]GameModel model)
        {
            var response = new ResponseViewModel();

            if (model == null)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { "Request data can not be null. " };
                return response;
            }

            var game = Mapper.Map<Game>(model);

            if (!string.IsNullOrWhiteSpace(model.GameDate))
                game.GameDate = Convert.ToDateTime(model.GameDate);

            if (model.Quarters.Any())
            {
                //var quarters = model.Quarters.Select(q => Mapper.Map<Quarter>(q));
                //game.Quarters.AddRange(quarters);

                foreach (var q in game.Quarters)
                {
                    q.Game = game;
                }
            }

            _gameRepository.Add(game);

            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[] { "Game added successfully" };
            response.Data = model;

            return response;
        }

        [Route("Game/Publish"), HttpGet]
        public ResponseViewModel Publish(int gameId)
        {
            var response = new ResponseViewModel();

            if (gameId == 0)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { "Request data does not have Game Id!" };
                return response;
            }

            try
            {
                _gameRepository.Publish(gameId);
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { ex.Message };
                return response;
            }

            response.StatusCode = HttpStatusCode.OK;
            response.Message = new [] { "Game published successfully."};
            return response;
        }

        [Route("Game/Unpublish"), HttpGet]
        public ResponseViewModel Unpublish(int gameId)
        {
            var response = new ResponseViewModel();

            if (gameId == 0)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { "Request data does not have Game Id!" };
                return response;
            }

            try
            {
                _gameRepository.Unpublish(gameId);
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { ex.Message };
                return response;
            }

            response.StatusCode = HttpStatusCode.OK;
            response.Message = new[] { "Game unpublished successfully." };
            return response;
        }

        private class Stat
        {
            public DateTime GameDate { get; set; }
            public decimal TotalFantasyPoints { get; set; }
        }
    }
}