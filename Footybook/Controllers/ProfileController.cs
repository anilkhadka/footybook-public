﻿using AutoMapper;
using Footybook.Services.Interfaces;
using Footybook.Web.Utilities;
using Footybook.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.IO;
using System.Threading.Tasks;

namespace Footybook.Web.Controllers
{
    [RoutePrefix("api/Profile")]
    public class ProfileController : ApiController
    {
        private readonly IProfileRepository _profileRepo;
        private readonly IIdentityRepository _identityRepo;
        private readonly IClubRepository _clubRepo;

        public ProfileController(IProfileRepository profileRepo, IIdentityRepository identityRepo, IClubRepository clubRepo)
        {
            _profileRepo = profileRepo;
            _identityRepo = identityRepo;
            _clubRepo = clubRepo;
        }

        [HttpPost, Route("Create")]
        public ResponseViewModel Update([FromBody]ProfileModel model)
        {
            var response = new ResponseViewModel();

            if (model == null)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Message = new string[] { "Request data can not be null." };
                return response;
            }

            var identity = (ClaimsIdentity)User.Identity;

            if (!identity.IsAuthenticated)
            {
                response.StatusCode = HttpStatusCode.Forbidden;
                response.Message = new string[] { "User not authorized." };
                return response;
            }

            var email = identity.Claims.SingleOrDefault(c => c.Type == ClaimTypes.Name);
            var userGuid = identity.Claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
            
            var user = _identityRepo.FindByEmail(email.Value);

            if (user == null)
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.Message = new string[] { "User not found." };
                return response;
            }

            var profile = user.Profile;

            if(model.Club != null)
                profile.ClubId = model.Club.Id;

            if(model.Division != null)
                profile.DivisionId = model.Division.Id;

            if (model.League != null)
                profile.LeagueId = model.League.Id;

            if (model.State != null)
                profile.StateId = model.State.Id;

            profile.Height = model.Height;
            profile.Identity = user;
            profile.Theme = model.Theme;
            profile.Weight = model.Weight;
            profile.Shoe = model.Shoe;
            profile.DOB = model.DOB;
            profile.LastModifiedDate = DateTime.UtcNow.ToFormattedDateTime("MM-dd-yyyy HH:mm:ss");
            profile.AgeGroup = model.AgeGroup;

            if (model.Identity != null)
            {
                profile.Identity.FirstName = model.Identity.FirstName;
                profile.Identity.LastName = model.Identity.LastName;
            }
            
            _profileRepo.Update(profile);

            var profileModel = Mapper.Map<ProfileModel>(user.Profile);

            if (model.Club != null)
            {
                var club = _clubRepo.GetClubById(model.Club.Id);

                if (club != null)
                {
                    profileModel.Club = new ClubModel
                    {
                        Id = club.Id,
                        ClubName = club.ClubName
                    };
                }
            }

            if (model.Division != null)
            {
                var division = _clubRepo.GetDivisionById(model.Division.Id);

                if (division != null)
                {
                    profileModel.Division = new DivisionModel
                    {
                        Id = division.Id,
                        DivisionName = division.DivisionName
                    };
                }
            }

            if (model.League != null)
            {
                var league = _clubRepo.GetLeagueById(model.League.Id);

                if (league != null)
                {
                    profileModel.League = new LeagueModel
                    {
                        Id = league.Id,
                        LeagueName = league.LeagueName
                    };
                }
            }

            if (model.State != null)
            {
                var state = _clubRepo.GetStateById(model.State.Id);

                if (state != null)
                {
                    profileModel.State = new StateModel
                    {
                        Id = state.Id,
                        Name = state.Name
                    };
                }
            }

            //response.Data = new { Profile = profileModel, Identity = new { FirstName = user.FirstName, LastName = user.LastName } };
            response.Data = new { Profile = profileModel };
            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[]{"Profile updated successfully."};

            return response;
        }

        [HttpPost, Route("Upload"), AllowAnonymous]
        public ResponseViewModel UploadProfileImage([FromBody] FileModel uploadImage)
        {
            var response = new ResponseViewModel();

            var identity = (ClaimsIdentity)User.Identity;

            if (!identity.IsAuthenticated)
            {
                response.StatusCode = HttpStatusCode.Forbidden;
                response.Message = new string[] { "User not authorized." };
                return response;
            }

            var email = identity.Claims.SingleOrDefault(c => c.Type == ClaimTypes.Name);
            var userGuid = identity.Claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            var user = _identityRepo.FindByEmail(email.Value);

            if (user == null)
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.Message = new string[] { "User not found." };
                return response;
            }

            //upload the image 
            var filePath = string.Empty;
            var fileName = string.Empty;

            if (uploadImage != null)
            {
                try
                {
                    fileName = string.Format("{0}{1}", System.Guid.NewGuid().ToString(), ".jpg");
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/ImageUploads"), fileName);
                    File.WriteAllBytes(filePath, uploadImage.Content);
                }
                catch (Exception ex)
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                    response.Message = new string[] { "Upload was not successful." };
                    return response;
                }
            }

            var profile = user.Profile;
            profile.ImageUrl = string.Format("{0}/{1}", "/ImageUploads", fileName);

            _profileRepo.Update(profile);

            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[] { "Profile Image uploaded successfully." };
            return response;
        }

        [HttpGet]
        public ResponseViewModel Get(int identityId)
        {
            var response = new ResponseViewModel();
            var user = _identityRepo.GetById(identityId);

            if (user == null)
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.Message = new string[] { "User not found." };
                return response;
            }
                
            var profile = _profileRepo.GetOrThrow(identityId);

            var profileModel = Mapper.Map<ProfileModel>(profile);

            if (profile.StateId != 0)
            {
                var state = _clubRepo.GetStateById(profile.StateId);
                profileModel.State = new StateModel { 
                    Id = state.Id,
                    Name = state.Name
                };
            }

            if (profile.LeagueId != 0)
            {
                var league = _clubRepo.GetLeagueById(profile.LeagueId);
                profileModel.League = new LeagueModel { 
                    Id = league.Id,
                    LeagueName = league.LeagueName
                };
            }

            if (profile.DivisionId != 0)
            {
                var division = _clubRepo.GetDivisionById(profile.DivisionId);
                profileModel.Division = new DivisionModel { 
                    Id = division.Id,
                    DivisionName = division.DivisionName
                };
            }

            if (profile.ClubId != 0)
            {
                var club = _clubRepo.GetClubById(profile.ClubId);
                profileModel.Club = new ClubModel { 
                    Id = club.Id,
                    ClubName = club.ClubName
                };
            }

            response.Data = new
            {
                Profile = profileModel,
                Identity = new { FirstName = profile.Identity.FirstName, LastName = profile.Identity.LastName }
            };
            response.StatusCode = HttpStatusCode.OK;
            response.Message = new string[] { "User profile found." };
            return response;
        }
    }
}