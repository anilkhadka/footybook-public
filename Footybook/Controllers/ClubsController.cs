﻿using AutoMapper;
using Footybook.Domain.Entities;
using Footybook.Services.Interfaces;
using Footybook.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Footybook.Web.Controllers
{
    [RoutePrefix("api")]
    public class ClubsController : ApiController
    {
        private readonly IClubRepository _clubRepo;

        public ClubsController(IClubRepository clubRepository)
        {
            _clubRepo = clubRepository;
        }

        [HttpGet, Route("Clubs")]
        public IEnumerable<ClubModel> GetClubsByDivisionId(int divisionId)
        {
            var clubModels = new List<ClubModel>();
            var clubs = _clubRepo.GetClubsByDivisionId(divisionId);

            if (clubs.Any())
            {
                clubModels.AddRange(clubs.Select(c => Mapper.Map<ClubModel>(c)));
            }

            return clubModels.OrderBy(cb => cb.ClubName);
        }

        [HttpGet, Route("Divisions")]
        public IEnumerable<DivisionModel> GetDivisionsByLeagueId(int leagueId)
        {
            var divisionModels = new List<DivisionModel>();
            var divisions = _clubRepo.GetDivisionsByLeagueId(leagueId);

            if(divisions.Any())
            {
                divisionModels.AddRange(divisions.Select(d => Mapper.Map<DivisionModel>(d)));
            }

            return divisionModels.OrderBy(dm => dm.DivisionName);
        }

        [HttpGet, Route("Leagues")]
        public IEnumerable<LeagueModel> GetLeaguesByStateId(int stateId)
        { 
            var leagueModels = new List<LeagueModel>();
            var leagues = _clubRepo.GetLeaguesByStateId(stateId);

            if (leagues.Any())
            {
                leagueModels.AddRange(leagues.Select(l => Mapper.Map<LeagueModel>(l)));
            }

            return leagueModels.OrderBy(lm => lm.LeagueName);
        }

        [HttpGet, Route("States")]
        public IEnumerable<StateModel> GetAllStates()
        {
            var stateModels = new List<StateModel>();
            var states = _clubRepo.GetAllStates();

            if (states.Any())
                stateModels.AddRange(states.Select(s => Mapper.Map<StateModel>(s)));

            return stateModels.OrderBy(sm => sm.Name);
        }
    }
}
