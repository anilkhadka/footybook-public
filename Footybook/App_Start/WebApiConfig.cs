﻿using AutoMapper;
using Castle.Windsor;
using FluentValidation.WebApi;
using Footybook.Domain.Entities;
using Footybook.Web.Filters;
using Footybook.Web.Infrastructure;
using Footybook.Web.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;

namespace Footybook.Web
{
    public static class WebApiConfig
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Register(HttpConfiguration config)
        {
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter("Bearer"));

            // Web API configuration and services
            // Initialize Castle & install application components
            var container = new WindsorContainer();
            container.Install(new WebApiInstaller());

            // Configure WebApi to use a new Windsor Castle DependencyResolver as its dependency resolver
            config.DependencyResolver = new Footybook.Web.Infrastructure.DependencyResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //Configure JsonMediaTypeFormatter
            //config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());

            //config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling =
            //    Newtonsoft.Json.PreserveReferencesHandling.All;

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;

            //Note: If there's need to add more media type formatters in the future, uncomment the following lines
            //config.Formatters.Add(new XmlMediaTypeFormatter());
            config.Formatters.Add(new FormUrlEncodedMediaTypeFormatter());
            config.Formatters.Add(new FormMultipartEncodedMediaTypeFormatter());

            //Register filters
            config.Filters.Add(new System.Web.Http.AuthorizeAttribute());
            config.Filters.Add(new ValidationFilterAttribute());

            //configure FluentValidation model validation provider
            FluentValidationModelValidatorProvider.Configure(config);

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //AutoMapper configuration
            Mapper.Initialize(cfg =>
            {
                //cfg.BindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
                cfg.CreateMap<IdentityModel, Identity>();
                cfg.CreateMap<Identity, IdentityModel>();
                cfg.CreateMap<ProfileModel, Footybook.Domain.Entities.Profile>();
                cfg.CreateMap<Footybook.Domain.Entities.Profile, ProfileModel>();
                cfg.CreateMap<Game, GameModel>();
                cfg.CreateMap<GameModel, Game>();
                cfg.CreateMap<Quarter, QuarterModel>();
                cfg.CreateMap<QuarterModel, Quarter>();
                cfg.CreateMap<IdentityUpdateModel, Identity>();
                cfg.CreateMap<Identity, IdentityUpdateModel>();
                cfg.CreateMap<State, StateModel>();
                cfg.CreateMap<StateModel, State>();
                cfg.CreateMap<Division, DivisionModel>();
                cfg.CreateMap<DivisionModel, Division>();
                cfg.CreateMap<League, LeagueModel>();
                cfg.CreateMap<LeagueModel, League>();
                cfg.CreateMap<Club, ClubModel>();
                cfg.CreateMap<ClubModel, Club>();
            });

            log.Info("Application started.");
        }
    }
}
