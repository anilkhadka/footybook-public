﻿using Footybook.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Tests.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task<ResponseViewModel> Identity_Create(IdentityModel model)
        {
            var responseViewModel = new ResponseViewModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49436/");
                //client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.PostAsJsonAsync<IdentityModel>("api/Identity/Register", model);

                if (response.IsSuccessStatusCode)
                {
                    responseViewModel = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                }
            }

            return responseViewModel;
        }
    }
}
