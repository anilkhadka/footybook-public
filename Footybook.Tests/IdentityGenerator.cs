﻿using Footybook.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Tests
{
    public static class IdentityGenerator
    {
        public static IdentityModel CreateIdentity()
        {
            return new IdentityModel { 
                Email = string.Format("{0}@abc.com",  new Random().Next(1, 50).ToString()),
                Password = "abc123",
                FirstName = "Anil",
                LastName = "Khadka"
            };
        }
    }
}
