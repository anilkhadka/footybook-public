﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Tests
{
    public static class ImageFileManger
    {
        public static byte[] GetTestImage()
        {
            var assemblyPath = Path.GetDirectoryName(Assembly.GetAssembly(typeof(ImageFileManger)).Location);

            var temp = assemblyPath.Split(new []{'\\'}, StringSplitOptions.None);

            var dirPath = string.Join("\\", temp.Take(temp.Count() - 2).ToArray());

            var filePath = string.Format("{0}{1}", dirPath, @"\ImageUploads\TestImage.jpg");

            return File.ReadAllBytes(filePath);
        }
    }
}
