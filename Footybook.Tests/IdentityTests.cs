﻿using Footybook.Domain.Entities;
using Footybook.Services.Interfaces;
using Footybook.Tests.Extensions;
using Footybook.Web.Controllers;
using Footybook.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace Footybook.Tests
{
    [TestClass]
    public class IdentityTests
    {
        private IdentityModel _identityModelToCreate;
               
        public IdentityTests()
        {
            _identityModelToCreate = IdentityGenerator.CreateIdentity();
        }

        [TestMethod]
        public async Task RegisterUser()
        {
            var response = await HttpClientExtensions.Identity_Create(_identityModelToCreate);

            if (response != null) 
            {
                var identityModel = (dynamic)response.Data;
                if (identityModel != null)
                {
                    Assert.AreEqual(_identityModelToCreate.Email, identityModel.email.ToString());
                    Assert.AreEqual(_identityModelToCreate.FirstName, identityModel.firstName.ToString());
                    Assert.AreEqual(_identityModelToCreate.LastName, identityModel.lastName.ToString());
                }
            }
        }
    }
}
