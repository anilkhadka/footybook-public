﻿using Footybook.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Tests
{
    public static class ProfileGenerator
    {
        public static ProfileModel CreateProfile()
        {
            return new ProfileModel
            {
                AgeGroup = 15,
                Club = new ClubModel
                {
                    Id = 1,
                    ClubName = "Fremantle Dockers"
                },
                Division = new DivisionModel {
                    Id = 1,
                    DivisionName = "1st Division"
                },
                DOB = DateTime.UtcNow.AddYears(-15),

            };
        }
    }
}
