﻿using Footybook.Domain.Database;
using Footybook.Domain.Entities;
using Footybook.Domain.Exceptions;
using Footybook.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Services.Impls
{
    public class ProfileRepository : IProfileRepository
    {
        public void Update(Profile profile)
        {
            if (profile == null)
                return;

            using (var session = DbSessionFactory.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    session.SaveOrUpdate(profile);
                    trans.Commit();
                }
            }
        }

        public Profile GetOrThrow(int id)
        {
            Profile profile = null;

            using (var session = DbSessionFactory.OpenSession())
            {
                profile = DbSessionFactory.OpenSession().Get<Profile>(id);

                if (profile == null)
                    throw new DataNotFoundException("Profile does not exist.");
                
            }

            return profile;
        }
    }
}
