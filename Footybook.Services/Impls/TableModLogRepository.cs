﻿using Footybook.Domain.Database;
using Footybook.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Services.Impls
{
    public class TableModLogRepository : ITableModLogRepository
    {
        public void Add(Domain.Entities.TableModLog tableModLog)
        {
            if (tableModLog == null)
                return;

            using (var session = DbSessionFactory.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    session.SaveOrUpdate(tableModLog);
                    trans.Commit();
                }
            }
        }


        public void Add(IEnumerable<Domain.Entities.TableModLog> tableModLogs)
        {
            if (tableModLogs == null || tableModLogs.Count() < 1)
                return;

            using (var statelessSession = DbSessionFactory.OpenStatelessSession())
            {
                using (var trans = statelessSession.BeginTransaction())
                {
                    statelessSession.Insert(tableModLogs);
                    trans.Commit();
                }
            }
        }
    }
}
