﻿using Footybook.Domain.Database;
using Footybook.Domain.Entities;
using Footybook.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;

namespace Footybook.Services.Impls
{
    public class ClubRepository : IClubRepository
    {
        public List<State> GetAllStates()
        {
            List<State> states = null;

            using (var session = DbSessionFactory.OpenSession())
            {
                states = session.Query<State>().ToList();
            }

            return states;
        }

        public List<Domain.Entities.League> GetAllLeagues()
        {
            List<League> leagues = null;
            
            using (var session = DbSessionFactory.OpenSession())
            {
                leagues = session.Query<League>().ToList();
            }

            return leagues;
        }

        public List<Domain.Entities.Division> GetAllDivisions()
        {
            List<Division> divisions = null;

            using (var session = DbSessionFactory.OpenSession())
            {
                divisions = session.Query<Division>().ToList();
            }

            return divisions;
        }

        public List<Domain.Entities.Club> GetAllClubs()
        {
            List<Club> clubs = null;

            using (var session = DbSessionFactory.OpenSession())
            {
                clubs = session.Query<Club>().ToList();
            }

            return clubs;
        }

        public List<Domain.Entities.Club> GetClubsByDivisionId(int divisionId)
        {
            List<Club> clubs = null;

            using (var session = DbSessionFactory.OpenSession())
            {
                clubs = session.Query<Club>()
                    .Where(c => c.Division.Id == divisionId)
                    .ToList();
            }

            return clubs;
        }

        public List<Domain.Entities.Division> GetDivisionsByLeagueId(int leagueId)
        {
            List<Division> divisions = null;

            using (var session = DbSessionFactory.OpenSession())
            {
                divisions = session.Query<Division>()
                    .Where(c => c.League.Id == leagueId)
                    .ToList();
            }

            return divisions;
        }

        public List<Domain.Entities.League> GetLeaguesByStateId(int stateId)
        {
            List<League> leagues = null;

            using (var session = DbSessionFactory.OpenSession())
            {
                leagues = session.Query<League>()
                    .Where(c => c.State.Id == stateId)
                    .ToList();
            }

            return leagues;
        }

        public State GetStateById(int stateId)
        {
            State state = null;
            using (var session = DbSessionFactory.OpenSession())
            {
                state = session.Query<State>()
                                  .SingleOrDefault(s => s.Id == stateId);
            }

            return state;
        }

        public League GetLeagueById(int leagueId)
        {
            League league = null;
            using (var session = DbSessionFactory.OpenSession())
            {
                league = session.Query<League>()
                                  .SingleOrDefault(s => s.Id == leagueId);
            }

            return league;
        }

        public Division GetDivisionById(int divisionId)
        {
            Division division = null;
            using (var session = DbSessionFactory.OpenSession())
            {
                division = session.Query<Division>()
                                  .SingleOrDefault(s => s.Id == divisionId);
            }

            return division;
        }

        public Club GetClubById(int clubId)
        {
            Club club = null;
            using (var session = DbSessionFactory.OpenSession())
            {
                club = session.Query<Club>()
                                  .SingleOrDefault(s => s.Id == clubId);
            }

            return club;
        }

        public List<State> GetStatesByIds(int[] ids)
        {
            var statesList = new List<State>();

            using (var session = DbSessionFactory.OpenSession())
            {
                statesList = session.Query<State>()
                                    .Where(s => ids.Contains(s.Id))
                                    .ToList();
            }

            return statesList;
        }
    }
}
