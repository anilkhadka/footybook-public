﻿using Footybook.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Footybook.Domain.Entities;
using Footybook.Domain.Database;

namespace Footybook.Services.Impls
{
    public class MoveLookUpRepository : IMoveLookUpRepository
    {
        public List<MoveLookUp> GetAll()
        {
            var lookUpList = new List<MoveLookUp>();

            using (var session = DbSessionFactory.OpenSession())
            {
                lookUpList = session.QueryOver<MoveLookUp>().List() as List<MoveLookUp>;
            }

            return lookUpList;
        }
    }
}
