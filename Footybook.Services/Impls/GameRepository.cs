﻿using Footybook.Domain.Database;
using Footybook.Domain.Entities;
using Footybook.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using NHibernate.Transform;
using NHibernate;

namespace Footybook.Services.Impls
{
    public class GameRepository : IGameRepository
    {
        public Game GetById(int id)
        {
            Game game = null;

            if (id == 0)
                return game;

            using (var session = DbSessionFactory.OpenSession())
            {
                game = session.Query<Game>()
                    .SingleOrDefault(g => g.Id == id);
            }

            return game;
        }

        public void Add(Game game)
        {
            if (game == null)
                return;

            using (var session = DbSessionFactory.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    session.SaveOrUpdate(game);
                    trans.Commit();
                }
            }

        }

        public void Update(Game game)
        {
            if (game == null)
                return;

            using (var session = DbSessionFactory.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    session.SaveOrUpdate(game);
                    trans.Commit();
                }
            }
        }

        public List<Game> GamesByIdentityId(int identityId, bool isCurrentUser)
        {
            using (var session = DbSessionFactory.OpenSession())
            {
                var query = session.Query<Game>()
                    .Where(g => g.IdentityId == identityId)
                    .ToList();

                if (!isCurrentUser)
                    query = query.Where(q => q.Publish).ToList();

                return query;
            }
        }

        public IList<StatsDto> GamesPerYearPerUser(int identityId)
        {
            using (var session = DbSessionFactory.OpenSession())
            {
                IQuery query = session.CreateSQLQuery("SELECT g.GameDate AS GameDate," + 
                "CAST(CONVERT(DECIMAL(18,2),SUM(q.Kicks)) AS DECIMAL(18, 2)) AS AverageKicks," + 
                "CAST((CONVERT(DECIMAL(18,2),SUM(q.Handballs))) AS DECIMAL(18, 2)) AS AverageHandballs," + 
                "CAST((CONVERT(DECIMAL(18, 2), SUM(q.Goals))) AS DECIMAL(18, 2)) AS AverageGoals," +
                "CAST((CONVERT(DECIMAL(18, 2), SUM(q.Behinds))) AS DECIMAL(18, 2)) AS AverageBehinds,"+
                "CAST((CONVERT(DECIMAL(18, 2), SUM(q.Tackles))) AS DECIMAL(18, 2)) AS AverageTackles," + 
                "CAST(CONVERT(DECIMAL(18, 2), (SUM(q.Kicks * 3)) +" + 
                "CONVERT(DECIMAL(18, 2), SUM(q.Marks * 3)) + " + 
                "CONVERT(DECIMAL(18, 2), SUM(q.Handballs * 2)) + " + 
                "CONVERT(DECIMAL(18, 2), SUM(q.Tackles * 4)) + " + 
                "CONVERT(DECIMAL(18, 2), SUM(q.HitOuts * 1)) + " + 
                "CONVERT(DECIMAL(18, 2), SUM(q.Goals * 6)) + " +
                "CONVERT(DECIMAL(18, 2), SUM(q.Behinds * 1)) + " +
                "CONVERT(DECIMAL(18, 2), SUM(q.FreeKicksFor * 1)) + " + 
                "CONVERT(DECIMAL(18, 2), SUM(q.FreeKicksAgainst * 3))) AS DECIMAL(18, 2)) AS AverageFantasyPoints," +
                "CAST(((SUM(q.Kicks) + SUM(q.Handballs) + SUM(q.HitOuts) + SUM(q.Behinds))) AS DECIMAL(18, 2)) AS Possessions " + 
                "FROM [dbo].[Identity] i " + 
                "JOIN [dbo].[Games] g ON g.IdentityId = i.Id " + 
                "JOIN [dbo].[Quarters] q ON q.GameId = g.Id " + 
                "WHERE i.Id = " + identityId + " GROUP BY g.GameDate").SetResultTransformer(Transformers.AliasToBean<StatsDto>());

                IList<StatsDto> statsDtoList =  query.List<StatsDto>();
                return statsDtoList;
            }
        }

        public List<Game> GamesByIdentityIds(int[] ids)
        {
            using (var session = DbSessionFactory.OpenSession())
            {
                var query = session.Query<Game>()
                    .Where(g => ids.Contains(g.IdentityId))
                    .ToList();

                return query;
            }
        }

        public List<Game> All()
        {
            var gameList = new List<Game>();

            using (var session = DbSessionFactory.OpenSession())
            {
                gameList = session.QueryOver<Game>().List() as List<Game>;
            }

            return gameList;
        }

        public void Publish(int gameId)
        {
            using (var session = DbSessionFactory.OpenSession())
            {
                var game = GetById(gameId);
                if (game == null)
                    throw new Exception("Game not found.");

                game.Publish = true;

                using (var trans = session.BeginTransaction())
                {
                    session.SaveOrUpdate(game);
                    trans.Commit();
                }
            }
        }

        public void Unpublish(int gameId)
        {
            using (var session = DbSessionFactory.OpenSession())
            {
                var game = GetById(gameId);
                if (game == null)
                    throw new Exception("Game not found.");

                game.Publish = false;
                using (var trans = session.BeginTransaction())
                {
                    session.SaveOrUpdate(game);
                    trans.Commit();
                }
            }
        }
    }
}
