﻿using Footybook.Domain.Database;
using Footybook.Domain.Entities;
using Footybook.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;

namespace Footybook.Services.Impls
{
    public class IdentityRepository : IIdentityRepository
    {
        public Identity Add(Identity identity)
        {
            if (identity == null)
                return null;

            using (var session = DbSessionFactory.OpenSession())
            {
                using (var trans = session.BeginTransaction()) 
                {
                    session.SaveOrUpdate(identity);
                    trans.Commit();
                }
            }

            return identity;
        }

        public Identity Update(Identity identity)
        {
            if (identity == null)
                return null;

            using (var session = DbSessionFactory.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    session.SaveOrUpdate(identity);
                    trans.Commit();
                }
            }

            return identity;
        }

        public Identity FindByEmail(string email)
        {
            Identity identity;
            
            using (var session = DbSessionFactory.OpenSession()) {
                identity = session.Query<Identity>()
                                  .SingleOrDefault(x => x.Email.ToLower() == email.ToLower());
            }

            return identity;
        }

        public Identity GetById(int id)
        { 
            Identity identity;
            using(var session = DbSessionFactory.OpenSession())
            {
                identity = session.Query<Identity>().SingleOrDefault(i => i.Id == id);
            }

            return identity;
        }

        public Identity GetByUserGuid(string id)
        {
            Identity identity = null;
            using (var session = DbSessionFactory.OpenSession())
            {
                identity = session.Query<Identity>()
                                  .Where(i => i.UserGuid.ToLower() == id.ToLower())
                                  .SingleOrDefault();
            }

            return identity;
        }

        //Get all identities by age group
        public List<Identity> GetAllUsersByAgeGroup(int ageGroup, string query)
        {
            var identityList = new List<Identity>();

            using (var session = DbSessionFactory.OpenSession())
            {
                if (ageGroup < 18)
                    identityList = session.Query<Profile>()
                        .Where(p => p.AgeGroup == ageGroup)
                        .Select(p => p.Identity)
                        .ToList();
                else
                    identityList = session.Query<Profile>()
                        .Where(p => p.AgeGroup >= 18)
                        .Select(p => p.Identity)
                        .ToList();
            }

            if (!string.IsNullOrWhiteSpace(query))
            {
                identityList = identityList
                    .Where(i => i.FirstName.Contains(query)
                    || i.LastName.Contains(query))
                    .ToList();
            }

            return identityList;
        }
    }
}
