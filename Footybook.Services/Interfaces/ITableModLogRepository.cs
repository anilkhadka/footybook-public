﻿using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Services.Interfaces
{
    public interface ITableModLogRepository
    {
        void Add(TableModLog tableModLog);
        void Add(IEnumerable<TableModLog> tableModLogs);
    }
}
