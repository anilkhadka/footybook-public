﻿using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Services.Interfaces
{
    public interface IIdentityRepository
    {
        Identity Add(Identity identity);
        Identity Update(Identity identity);
        Identity FindByEmail(string email);
        Identity GetByUserGuid(string userGuid);
        Identity GetById(int id);
        List<Identity> GetAllUsersByAgeGroup(int ageGroup, string query);
    }
}
