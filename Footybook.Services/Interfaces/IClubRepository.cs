﻿using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Services.Interfaces
{
    public interface IClubRepository
    {
        List<State> GetAllStates();

        List<League> GetAllLeagues();

        List<Division> GetAllDivisions();

        List<Club> GetAllClubs();

        List<Club> GetClubsByDivisionId(int divisionId);

        List<Division> GetDivisionsByLeagueId(int leagueId);

        List<League> GetLeaguesByStateId(int stateId);

        State GetStateById(int stateId);

        League GetLeagueById(int leagueId);

        Division GetDivisionById(int divisionId);

        Club GetClubById(int clubId);

        List<State> GetStatesByIds(int[] ids);
    }
}
