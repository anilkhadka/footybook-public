﻿using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Services.Interfaces
{
    public interface IGameRepository
    {
        Game GetById(int id);

        void Add(Game game);

        void Update(Game game);

        List<Game> GamesByIdentityId(int identityId, bool isCurrentUser);

        IList<StatsDto> GamesPerYearPerUser(int identityId);

        List<Game> GamesByIdentityIds(int[] ids);

        List<Game> All();

        void Publish(int gameId);

        void Unpublish(int gameId);
    }
}
