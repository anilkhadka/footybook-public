﻿using Footybook.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Footybook.Services.Interfaces
{
    public interface IProfileRepository
    {
        void Update(Profile profile);

        Profile GetOrThrow(int id);
    }
}
