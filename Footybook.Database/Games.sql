﻿CREATE TABLE [dbo].[Games]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [IdentityId] INT NOT NULL, 
    [Round] NVARCHAR(50) NOT NULL, 
    [RecorderName] NVARCHAR(50) NOT NULL, 
    [RecorderEmail] NVARCHAR(50) NOT NULL, 
    [GameDate] DATE NOT NULL, 
    [Publish] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Games_IdentityId_To_Identity_Id] FOREIGN KEY ([IdentityId]) REFERENCES [dbo].[Identity]([Id])
)
