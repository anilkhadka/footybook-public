﻿CREATE TABLE [dbo].[Divisions]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DivisionName] NVARCHAR(250) NOT NULL, 
    [LeagueId] INT NOT NULL, 
    CONSTRAINT [FK_LeagueId_To_League_Id] FOREIGN KEY ([LeagueId]) REFERENCES [dbo].[Leagues]([Id])
)
