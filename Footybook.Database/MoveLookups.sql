﻿CREATE TABLE [dbo].[MoveLookups]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [MoveType] NVARCHAR(50) NOT NULL, 
    [Points] INT NOT NULL
)
