﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [dbo].[Identity] (Table)
       [dbo].[Log] (Table)
       [dbo].[Profile] (Table)
       Default Constraint: unnamed constraint on [dbo].[Identity] (Default Constraint)
       Default Constraint: unnamed constraint on [dbo].[Identity] (Default Constraint)
       Default Constraint: unnamed constraint on [dbo].[Profile] (Default Constraint)
       Default Constraint: unnamed constraint on [dbo].[Profile] (Default Constraint)
       [dbo].[FK_Profile_IdentityId_Identity_Id] (Foreign Key)

** Supporting actions
