﻿** Highlights
     Tables that will be rebuilt
       [dbo].[Identity]
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       The type for column Salt in table [dbo].[Identity] is currently  NVARCHAR (20) NOT NULL but is being changed to  BINARY
         (50) NOT NULL. Data loss could occur.

** User actions
     Drop
       unnamed constraint on [dbo].[Identity] (Default Constraint)
       unnamed constraint on [dbo].[Identity] (Default Constraint)
     Table rebuild
       [dbo].[Identity] (Table)

** Supporting actions
     Drop
       [dbo].[FK_Profile_IdentityId_Identity_Id] (Foreign Key)
     Create
       [dbo].[FK_Profile_IdentityId_Identity_Id] (Foreign Key)

The type for column Salt in table [dbo].[Identity] is currently  NVARCHAR (20) NOT NULL but is being changed to  BINARY (50) NOT NULL. Data loss could occur.

