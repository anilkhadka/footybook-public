﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       The type for column DOB in table [dbo].[Profile] is currently  NVARCHAR (50) NULL but is being changed to  DATETIME
         NULL. Data loss could occur.

** User actions
     Alter
       [dbo].[Profile] (Table)

** Supporting actions

The type for column DOB in table [dbo].[Profile] is currently  NVARCHAR (50) NULL but is being changed to  DATETIME NULL. Data loss could occur.

