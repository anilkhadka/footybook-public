﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [dbo].[Clubs] (Table)
       [dbo].[Divisions] (Table)
       [dbo].[Games] (Table)
       [dbo].[Identity] (Table)
       [dbo].[Leagues] (Table)
       [dbo].[Log] (Table)
       [dbo].[MoveLookups] (Table)
       [dbo].[Profile] (Table)
       [dbo].[Quarters] (Table)
       [dbo].[States] (Table)
       [dbo].[TableModLog] (Table)
       Default Constraint: unnamed constraint on [dbo].[Games] (Default Constraint)
       Default Constraint: unnamed constraint on [dbo].[Identity] (Default Constraint)
       Default Constraint: unnamed constraint on [dbo].[Identity] (Default Constraint)
       Default Constraint: unnamed constraint on [dbo].[Profile] (Default Constraint)
       Default Constraint: unnamed constraint on [dbo].[Profile] (Default Constraint)
       [dbo].[FK_DivisionId_To_Divisions_Id] (Foreign Key)
       [dbo].[FK_LeagueId_To_League_Id] (Foreign Key)
       [dbo].[FK_Games_IdentityId_To_Identity_Id] (Foreign Key)
       [dbo].[FK_StateId_To_State_Id] (Foreign Key)
       [dbo].[FK_Profile_IdentityId_Identity_Id] (Foreign Key)
       [dbo].[FK_Quarters_GameId_To_Games_Id] (Foreign Key)

** Supporting actions
