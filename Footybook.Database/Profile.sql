﻿CREATE TABLE [dbo].[Profile]
(
	[IdentityId] INT NOT NULL PRIMARY KEY, 
    [ImageUrl] NVARCHAR(250) NULL, 
    [Height] INT NULL, 
    [ClubId] INT NULL, 
    [DivisionId] INT NULL, 
    [LeagueId] INT NULL, 
    [Theme] NVARCHAR(250) NULL, 
    [CreatedDate] DATETIME NOT NULL DEFAULT GETDATE(), 
    [LastModifiedDate] DATETIME NULL, 
    [DOB] DATETIME NULL, 
    [Weight] REAL NULL, 
    [Shoe] NVARCHAR(250) NULL, 
    [StateId] INT NULL, 
    [Active] BIT NULL DEFAULT 1, 
    [AgeGroup] INT NULL, 
    CONSTRAINT [FK_Profile_IdentityId_Identity_Id] FOREIGN KEY ([IdentityId]) REFERENCES [dbo].[Identity]([Id]) 
	ON DELETE CASCADE
)
