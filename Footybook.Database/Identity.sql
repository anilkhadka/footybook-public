﻿CREATE TABLE [dbo].[Identity]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [UserGuid] NVARCHAR(10) NOT NULL, 
    [Email] NVARCHAR(250) NOT NULL, 
    [Password] NVARCHAR(100) NOT NULL, 
    [Salt] NVARCHAR(50) NOT NULL, 
    [FirstName] NVARCHAR(50) NOT NULL, 
    [LastName] NVARCHAR(50) NOT NULL, 
    [FacebookId] NVARCHAR(50) NULL, 
    [CreatedDate] DATETIME NOT NULL DEFAULT GETDATE(), 
    [LastModifiedDate] DATETIME NULL, 
    [Active] BIT NOT NULL DEFAULT 0, 
    [TempPassword] NVARCHAR(50) NULL
)
