﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


-- Data Insertion in dbo.State Table --

Use [Footybook]
GO

INSERT INTO dbo.States(Name) VALUES('WA');

INSERT INTO dbo.States(Name) VALUES('QLD');

GO

-- Data Insertion in dbo.League Table --
INSERT INTO dbo.Leagues(LeagueName, StateId) VALUES('Eastern Football League', 1);
INSERT INTO dbo.Leagues(LeagueName, StateId) VALUES('Essendon District Football League', 1);
GO


-- Data Insertion in dbo.Division Table --
INSERT INTO dbo.Divisions(DivisionName, LeagueId) VALUES('Division1', 1);

INSERT INTO dbo.Divisions(DivisionName, LeagueId) VALUES('Division2', 1);

GO

-- Data Insertion in dbo.Club Table --
INSERT INTO dbo.Clubs(ClubName, DivisionId) VALUES('Balwyn Football Club', 1);

INSERT INTO dbo.Clubs(ClubName, DivisionId) VALUES('Blackburn Football Club', 1);

GO

-- Data Insertion in dbo.MoveLookups Table --
INSERT INTO dbo.MoveLookups(MoveType, Points) VALUES('Kicks', 3);

INSERT INTO dbo.MoveLookups(MoveType, Points) VALUES('Handballs', 2);

INSERT INTO dbo.MoveLookups(MoveType, Points) VALUES('Marks', 3);

INSERT INTO dbo.MoveLookups(MoveType, Points) VALUES('HitOuts', 1);

INSERT INTO dbo.MoveLookups(MoveType, Points) VALUES('Goals', 6);

INSERT INTO dbo.MoveLookups(MoveType, Points) VALUES('Behinds', 1);

INSERT INTO dbo.MoveLookups(MoveType, Points) VALUES('FreeKicksFor', 1);

INSERT INTO dbo.MoveLookups(MoveType, Points) VALUES('FreeKicksAgainst', 3);

INSERT INTO dbo.MoveLookups(MoveType, Points) VALUES('Tackles', 4);

GO



