﻿CREATE TABLE [dbo].[Leagues]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [LeagueName] NVARCHAR(250) NOT NULL, 
    [StateId] INT NOT NULL, 
    CONSTRAINT [FK_StateId_To_State_Id] FOREIGN KEY ([StateId]) REFERENCES [dbo].[States]([Id])
)
