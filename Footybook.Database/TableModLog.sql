﻿CREATE TABLE [dbo].[TableModLog]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [TableName] NVARCHAR(50) NULL, 
    [ColumnName] NVARCHAR(50) NULL, 
    [OldVal] NVARCHAR(50) NULL, 
    [NewValue] NVARCHAR(50) NULL, 
    [UserGuid] NVARCHAR(50) NULL, 
    [ModifiedDateTime] DATETIME NULL, 
    [IPAddress] NVARCHAR(50) NULL, 
    [OperatingSystem] NVARCHAR(50) NULL
)
