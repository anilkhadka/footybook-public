﻿CREATE TABLE [dbo].[Quarters]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [QuarterNo] INT NOT NULL, 
    [Kicks] INT NULL, 
    [Handballs] INT NULL, 
    [Marks] INT NULL, 
    [Tackles] INT NULL, 
    [HitOuts] INT NULL, 
    [Goals] INT NULL, 
    [Behinds] INT NULL, 
    [FreeKicksFor] INT NULL, 
    [FreeKicksAgainst] INT NULL, 
    [Comments] NVARCHAR(MAX) NULL, 
    [GameId] INT NOT NULL, 
    CONSTRAINT [FK_Quarters_GameId_To_Games_Id] FOREIGN KEY ([GameId]) REFERENCES [dbo].[Games]([Id])
)
