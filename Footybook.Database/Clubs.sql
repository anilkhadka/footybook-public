﻿CREATE TABLE [dbo].[Clubs]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ClubName] NVARCHAR(250) NOT NULL, 
    [DivisionId] INT NOT NULL, 
    CONSTRAINT [FK_DivisionId_To_Divisions_Id] FOREIGN KEY ([DivisionId]) REFERENCES [dbo].[Divisions]([Id])
)
